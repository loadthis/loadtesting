﻿USE master;
PRINT '--------------------------------------------------------------------------------'
PRINT '-- SQL Version information for current instance  (Version Info)'
SELECT @@VERSION AS [SQL Server and OS Version Info];

PRINT '--------------------------------------------------------------------------------'
PRINT '-- Get selected server properties (Server Properties)'
SELECT SERVERPROPERTY('MachineName') AS [MachineName];
SELECT SERVERPROPERTY('ServerName') AS [ServerName];
SELECT SERVERPROPERTY('InstanceName') AS [Instance]; 
SELECT SERVERPROPERTY('IsClustered') AS [IsClustered];
SELECT SERVERPROPERTY('ComputerNamePhysicalNetBIOS') AS [ComputerNamePhysicalNetBIOS];
SELECT SERVERPROPERTY('Edition') AS [Edition];
SELECT SERVERPROPERTY('ProductLevel') AS [ProductLevel];
SELECT SERVERPROPERTY('ProductVersion') AS [ProductVersion];
SELECT SERVERPROPERTY('Collation') AS [Collation];

PRINT '--------------------------------------------------------------------------------'
PRINT '-- Returns a list of all global trace flags that are enabled (Global Trace Flags)'
DBCC TRACESTATUS (-1);

PRINT '--------------------------------------------------------------------------------'
PRINT '-- Hardware Information from SQL Server 2005 (Hardware Info)'
-- (Cannot distinguish between HT and multi-core)
SELECT cpu_count AS [Logical CPU Count], hyperthread_ratio AS [Hyperthread Ratio],
cpu_count/hyperthread_ratio AS [Physical CPU Count], 
physical_memory_in_bytes/1048576 AS [Physical Memory (MB)]
FROM sys.dm_os_sys_info WITH (NOLOCK) OPTION (RECOMPILE);

PRINT '--------------------------------------------------------------------------------'
PRINT '-- Get configuration values for instance  (Query 10) (Configuration Values)'
SELECT 
	name, 
	cast(value as Bigint) value, 
	cast(value_in_use as Bigint) value_in_use, 
	cast(minimum as Bigint) minimum, 
	cast(maximum as Bigint) maximum, 
	[description], is_dynamic, is_advanced
FROM sys.configurations WITH (NOLOCK)
ORDER BY name OPTION (RECOMPILE);

PRINT '--------------------------------------------------------------------------------'
PRINT '-- File Names and Paths for TempDB and all user databases in instance (Database Filenames and Paths)'
SELECT 
       CONVERT(nvarchar(50), DB_NAME([database_id])) AS [Database Name], 
       [file_id], 
	   CONVERT(nvarchar(50), name) name, 
	   CONVERT(nvarchar(90), physical_name) physical_name, type_desc, state_desc,
	   is_percent_growth, growth,
	   CONVERT(bigint, growth/128.0) AS [Growth in MB], 
       CONVERT(bigint, size/128.0) AS [Total Size in MB]
FROM sys.master_files WITH (NOLOCK)
WHERE [database_id] > 4 
AND [database_id] <> 32767
OR [database_id] = 2
ORDER BY DB_NAME([database_id]) OPTION (RECOMPILE);

PRINT '--------------------------------------------------------------------------------'
PRINT '-- Диски и свободное место на них'
exec xp_fixeddrives

PRINT '--------------------------------------------------------------------------------'
PRINT '-- статистика io по файлам БД'
select 
       CONVERT(nvarchar(50), DB_NAME(mf.[database_id])) AS [Database Name], 
       mf.[file_id], CONVERT(nvarchar(5), mf.type_desc) type_desc,
	   CONVERT(nvarchar(50), mf.name) name, 
	   vf.TimeStamp, 
	   uptime.uptime,
	   vf.NumberReads, vf.BytesRead, 
	   CAST(vf.IoStallReadMS/1000.0 as numeric(12,3)) IoStallRead, 
	   vf.NumberWrites, vf.BytesWritten, 
	   CAST(vf.IoStallWriteMS/1000.0 as numeric(12,3)) IoStallWrite, 
	   CAST(vf.IoStallMS /1000.0 as numeric(12,3)) IoStall, 
	   CAST(vf.BytesOnDisk/1024.0/1024.0 as numeric(12,3)) MB,
	   CAST(vf.IoStallMS*1.0 / (vf.NumberReads + vf.NumberWrites) as numeric(12,5))  average_io,
	   CAST(vf.IoStallReadMS / (vf.NumberReads + 1.0 ) as numeric(12,5))  average_read,
	   CAST(vf.IoStallWriteMS / (vf.NumberWrites + 1.0 ) as numeric(12,5))  average_write,
	   CAST(vf.IoStallMS*1.0 / (uptime.uptime) as numeric(12,5))  relative_io,
	   CAST(vf.IoStallReadMS / (uptime.uptime) as numeric(12,5))  relative_read,
	   CAST(vf.IoStallWriteMS / (uptime.uptime) as numeric(12,5))  relative_write,
	   CAST(vf.IoStallReadMS / (vf.IoStallMS*1.0) as numeric(12,5))  percent_read,
	   CAST(vf.IoStallWriteMS / (vf.IoStallMS*1.0) as numeric(12,5))  percent_write
from 
	::fn_virtualfilestats(null, null) vf
	inner join sys.master_files mf WITH (NOLOCK) on vf.DbId = mf.database_id and vf.FileId = mf.file_id
	cross apply (select cast (si.ms_ticks as numeric(30, 5)) uptime from sys.dm_os_sys_info si (nolock)) uptime
order by vf.IoStallMS desc
OPTION (RECOMPILE);

PRINT '--------------------------------------------------------------------------------'
PRINT '-- check that dm_os_wait_stats is correct'
select 
	w.wait_time_ms,
	(select si.ms_ticks from sys.dm_os_sys_info si (nolock)) uptime
from 
	sys.dm_os_wait_stats w WITH (NOLOCK)
where w.wait_type in (N'LAZYWRITER_SLEEP')
OPTION (RECOMPILE);


PRINT '--------------------------------------------------------------------------------'
PRINT '-- dm_os_wait_stats'
select 
	w.wait_type, 
	w.waiting_tasks_count,
	w.wait_time_ms,
	--w.max_wait_time_ms,
	--w.signal_wait_time_ms,
	--(select ms_ticks from sys.dm_os_sys_info si (nolock)) uptime_ms, 
	cast ((w.wait_time_ms*1.0)/w.waiting_tasks_count as numeric(30, 5)) avg_wait_time,
	--cast ((w.signal_wait_time_ms*1.0)/w.waiting_tasks_count as numeric(30, 5)) avg_signal_wait_time,
	cast ((w.wait_time_ms*100.0)/(select si.ms_ticks from sys.dm_os_sys_info si (nolock)) as numeric(30, 5)) wait_relative_to_uptime_percent
from 
	sys.dm_os_wait_stats w WITH (NOLOCK)
where 
	w.waiting_tasks_count>0 and 
	w.wait_time_ms>0 and 
	w.wait_type not in (
        N'BROKER_EVENTHANDLER', N'BROKER_RECEIVE_WAITFOR', N'BROKER_TASK_STOP',
		N'BROKER_TO_FLUSH', N'BROKER_TRANSMITTER', N'CHECKPOINT_QUEUE',
        N'CHKPT', N'CLR_AUTO_EVENT', N'CLR_MANUAL_EVENT', N'CLR_SEMAPHORE',
        N'DBMIRROR_DBM_EVENT', N'DBMIRROR_EVENTS_QUEUE', N'DBMIRROR_WORKER_QUEUE',
		N'DBMIRRORING_CMD', N'DIRTY_PAGE_POLL', N'DISPATCHER_QUEUE_SEMAPHORE',
        N'EXECSYNC', N'FSAGENT', N'FT_IFTS_SCHEDULER_IDLE_WAIT', N'FT_IFTSHC_MUTEX',
        N'HADR_CLUSAPI_CALL', N'HADR_FILESTREAM_IOMGR_IOCOMPLETION', N'HADR_LOGCAPTURE_WAIT', 
		N'HADR_NOTIFICATION_DEQUEUE', N'HADR_TIMER_TASK', N'HADR_WORK_QUEUE',
        N'KSOURCE_WAKEUP', N'LAZYWRITER_SLEEP', N'LOGMGR_QUEUE', N'ONDEMAND_TASK_QUEUE',
        N'PWAIT_ALL_COMPONENTS_INITIALIZED', N'QDS_PERSIST_TASK_MAIN_LOOP_SLEEP',
        N'QDS_CLEANUP_STALE_QUERIES_TASK_MAIN_LOOP_SLEEP', N'REQUEST_FOR_DEADLOCK_SEARCH',
		N'RESOURCE_QUEUE', N'SERVER_IDLE_CHECK', N'SLEEP_BPOOL_FLUSH', N'SLEEP_DBSTARTUP',
		N'SLEEP_DCOMSTARTUP', N'SLEEP_MASTERDBREADY', N'SLEEP_MASTERMDREADY',
        N'SLEEP_MASTERUPGRADED', N'SLEEP_MSDBSTARTUP', N'SLEEP_SYSTEMTASK', N'SLEEP_TASK',
        N'SLEEP_TEMPDBSTARTUP', N'SNI_HTTP_ACCEPT', N'SP_SERVER_DIAGNOSTICS_SLEEP',
		N'SQLTRACE_BUFFER_FLUSH', N'SQLTRACE_INCREMENTAL_FLUSH_SLEEP', N'SQLTRACE_WAIT_ENTRIES',
		N'WAIT_FOR_RESULTS', N'WAITFOR', N'WAITFOR_TASKSHUTDOWN', N'WAIT_XTP_HOST_WAIT',
		N'WAIT_XTP_OFFLINE_CKPT_NEW_LOG', N'WAIT_XTP_CKPT_CLOSE', N'XE_DISPATCHER_JOIN',
        N'XE_DISPATCHER_WAIT', N'XE_TIMER_EVENT')

ORDER BY 
	w.wait_time_ms DESC
OPTION (RECOMPILE);

PRINT '--------------------------------------------------------------------------------'
PRINT '-- dm_os_latch_stats'
SELECT 
	l.latch_class, 
	l.waiting_requests_count,
	l.wait_time_ms,
	cast ((l.wait_time_ms*1.0)/l.waiting_requests_count as numeric(30, 5)) avg_wait_time,
	cast ((l.wait_time_ms*100.0)/(SELECT si.ms_ticks FROM sys.dm_os_sys_info si (NOLOCK)) as numeric(30, 5)) wait_relative_to_uptime_percent
FROM 
	sys.dm_os_latch_stats l WITH (NOLOCK)
WHERE 
	l.waiting_requests_count > 0 and 
	l.wait_time_ms > 0 
	
ORDER BY 
	l.wait_time_ms DESC
OPTION (RECOMPILE);